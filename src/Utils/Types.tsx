import { Vector3 } from "@babylonjs/core";

export interface Animal {
  id:number;
  key:string;
  model:string;
  level:number;
  location:string,
  latitude:number,
  longitude:number,
  speciesThreat:string;
  couseOfHazard:string[];
  supportingOrganizations:SupportingOrganizations[];
  anwsers:number[]
}

export interface SupportingOrganizations{
  name:string;
  url:string;
}

export interface ShopItem {
  name:String;
  price: number;
  count: number;
  criteria: ShopItemCriteria;
}

type AnimalKey = keyof Animal;

export interface ShopItemCriteria {
  name:AnimalKey;
  value:string;
}


export interface Quest{
  scene:Scene[];
  activeScene:number;
  active:boolean;
}

export interface Scene{
  latitude:number;
  longitude:number;
  model:string;
  ar:AR;
}

export interface AR{
  models:ARmodel[];
  dialog:string;
}

export interface ARmodel{
  position: Position;
  scale:number;
  model:string;
}

export interface Position{
  x:number;
  y:number;
  z:number;
}
