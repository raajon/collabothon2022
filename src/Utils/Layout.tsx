import React from 'react';
import { useTranslation } from 'react-i18next';
import { useLocation, useNavigate } from "react-router-dom";
import { Outlet, Link } from "react-router-dom";
import { Container, Image, Nav, Navbar} from 'react-bootstrap';

import styles from './Layout.module.css'
import logo from '../Assets/img/logo.svg';
import Manual from '../Components/Manual/Manual';

const Camera = (props: React.PropsWithChildren<Props>) => {

  const { t } = useTranslation();
  const navigate = useNavigate();
  const location = useLocation();

  const goHome = () =>{
    navigate('/');
  }

  return (
    <>
      {location.pathname !== '/' && <Image src={logo} className={styles.logomini} alt="ZooGo Logo" onClick={goHome}/>}
      <Container className={styles.container}>
        <Outlet/>
      </Container>
      <br/>

      <Manual/>

      <Navbar bg="dark" variant="dark" fixed="bottom">
        <Container>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link as={Link} to="/map">{t('ui.map')}</Nav.Link>
              <Nav.Link as={Link} to="/gallery">{t('ui.gallery')}</Nav.Link>
              <Nav.Link as={Link} to="/achievements">{t('ui.achievements')}</Nav.Link>
              <Nav.Link as={Link} to="/shop">{t('ui.shop')}</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  )
}


interface Props {}

export default Camera;
