import { Vector3 } from "@babylonjs/core";

const mapCordinates = {
  maxlat: 51.7638903,
  maxlon: 19.4159373,
  minlat: 51.7580958,
  minlon: 19.4087522
}

export const global2local = (lat:number, lon:number):Vector3 => {
  const ratioZ = 10000;
  const diffLat = mapCordinates.maxlat - mapCordinates.minlat;
  const diffLon = mapCordinates.maxlon - mapCordinates.minlon;
  const ratio = diffLat/diffLon;
  const distLat = getDistance(mapCordinates.minlon, mapCordinates.minlon, mapCordinates.minlat, mapCordinates.maxlat );
  const distLon = getDistance(mapCordinates.minlon, mapCordinates.maxlon, mapCordinates.minlat, mapCordinates.minlat );
  const ratioX =( ratioZ / (distLat/distLon));

  const centerX = mapCordinates.minlon + (mapCordinates.maxlon-mapCordinates.minlon)/2
  const x = (lon - centerX)*ratioX *ratio;

  const centerZ = mapCordinates.minlat + (mapCordinates.maxlat-mapCordinates.minlat)/2
  const z = (lat - centerZ)*ratioZ;

  return new Vector3(x, 0, z);
}

export const getDistance = (lng1:number, lng2:number, lat1:number, lat2:number):number => {
 const earthRadius = 6371000;
 const pi180 =  Math.PI / 180;
 const diffLng = lng1 - lng2;
 const diffLat = lat1 - lat2;

 const a = Math.sin(pi180*diffLat/2) * Math.sin(pi180*diffLat/2) + Math.cos(pi180*lat2) * Math.cos(pi180*lat1) * Math.sin(pi180*diffLng/2) * Math.sin(pi180*diffLng/2)
 const b = 2 * Math.asin(Math.sqrt(a));
 return earthRadius * b;
}
