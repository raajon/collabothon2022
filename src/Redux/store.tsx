import { configureStore } from '@reduxjs/toolkit'
import animalReducer from './animalSlice'
import questReducer from './questSlice'

export default configureStore({
  reducer: {
    animalData: animalReducer,
    questData: questReducer
  },
})
