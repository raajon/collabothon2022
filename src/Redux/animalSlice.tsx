import { createSlice } from '@reduxjs/toolkit'
import { deepClone } from '../Utils/Helpers';
import animalData from '../Assets/AnimalData.json';
import { Animal } from '../Utils/Types';

let animalDataStored:Animal[] = JSON.parse(localStorage.getItem("animalData") || "[]");
let animalDataInit:Animal[] = deepClone(animalData)
animalDataInit.forEach(ad => {
  const a = animalDataStored.find((a)=> a.id===ad.id)
  ad.level = a?.level || 0;
});

export const counterSlice = createSlice({
  name: 'animalData',
  initialState: {
    animalData: animalDataInit,
  },
  reducers: {
    save: (state, action) => {
      state.animalData = action.payload;
      localStorage.setItem("animalData", JSON.stringify(state.animalData));
    },
    upgradeAnimal: (state, action) => {
      const id = action.payload;
      const animalToUpgrade = state.animalData.find(ad=>ad.id===id);
      if(animalToUpgrade){
        animalToUpgrade.level++;
      }
      localStorage.setItem("animalData", JSON.stringify(state.animalData));
    }
  },
})

export const { save, upgradeAnimal } = counterSlice.actions

export default counterSlice.reducer
