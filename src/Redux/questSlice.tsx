import { createSlice } from '@reduxjs/toolkit';
import { Quest } from '../Utils/Types';

let questDataInit:Quest = {
  active: true,
  activeScene:1,
  scene:[
  {latitude:51.7631003, longitude:19.4128777, model:"orangutan", ar:{models:[{position:{x:0, y:0, z:3}, scale:.5, model:"orangutan"}], dialog:"quest.orangutan.1"}},
    {latitude:51.763258, longitude:19.412671, model:"orangutan", ar:{models:[{position:{x:0, y:0, z:3}, scale:.5, model:"orangutan"}], dialog:"quest.orangutan.1"}},
    {latitude:51.76298275, longitude:19.4128275, model:"orangutan", ar:{models:[{position:{x:0, y:0, z:3}, scale:.2, model:"orangutan"}, {position:{x:0, y:0, z:5}, scale:.5, model:"buldozer"}], dialog:"quest.orangutan.2"}},
    {latitude:51.763258, longitude:19.412671, model:"orangutan", ar:{models:[{position:{x:-.3, y:0, z:3}, scale:.5, model:"orangutan"},{position:{x:.3, y:0, z:3.2}, scale:.2, model:"orangutan"}], dialog:"quest.orangutan.3"}}
  ]
}


export const questSlice = createSlice({
  name: 'questData',
  initialState: {
    questData: questDataInit,
  },
  reducers: {
    nextQuestPart: (state) => {
      state.questData.activeScene++;
      if(state.questData.scene.length === state.questData.activeScene){
        state.questData.active = false;
      }
    }
  },
})

export const { nextQuestPart } = questSlice.actions

export default questSlice.reducer
