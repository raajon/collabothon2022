import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import store from './Redux/store'
import { Provider } from 'react-redux'
import i18next from "i18next";
import { initReactI18next } from "react-i18next";


const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);


const detectionOptions = {
  order: ["queryString", "cookie", "localStorage"],
  lookupFromPathIndex: 0,
  lookupQuerystring: "lng",
  lookupCookie: 'i18next',
  lookupLocalStorage: 'i18nextLng'
};

i18next
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    // the translations
    // (tip move them in a JSON file and import them,
    // or even better, manage them via a UI: https://react.i18next.com/guides/multiple-translation-files#manage-your-translations-with-a-management-gui)
    resources: {
      en: {
        translation: require("./Assets/i18n/en.json"),
      },
      pl:{
        translation: require("./Assets/i18n/pl.json"),
      }
    },
    fallbackLng: "en",
    debug: true,
    detection: detectionOptions,
    // missingKeyHandler: (lngs, ns, key, fallbackValue, updateMissing, options)=>{console.log("missing key", lngs, ns, key, fallbackValue, updateMissing, options)},

    interpolation: {
      escapeValue: false // react already safes from xss => https://www.i18next.com/translation-function/interpolation#unescape
    },
  });


// i18next.use(LanguageDetector).init({
//   fallbackLng: "en",
//   resources: {
//     en: {
//       translation: require("./Assets/i18n/en.json"),
//     },
//     pl: {
//       translation: require("./Assets/i18n/pl.json"),
//     },
//   },
//   // ns: ["common"],
//   // defaultNS: "common",
//   detection: detectionOptions,
//   // returnObjects: true,
//   debug: true,
//   // interpolation: {
//   //   escapeValue: false, // not needed for react!!
//   // },
//   // react: {
//   //   wait: true,
//   // },
// });

// i18next.languages = ["pl", "en"];

root.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
