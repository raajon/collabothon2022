import React from 'react';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';

import "@babylonjs/inspector";
import "@babylonjs/loaders/glTF";

import Map from './Pages/Map';
import Camera from './Pages/Camera';
import Gallery from './Pages/Gallery';
import Achievements from './Pages/Achievements';
import AnimalDetail from './Pages/AnimalDetail';
import Catch from './Pages/Catch';
import AR from './Pages/AR';
import ARquest from './Pages/ARquest';
import Shop from './Pages/Shop';
import Wellcome from './Pages/Wellcome';
import Layout from './Utils/Layout';

import styles from './App.module.css'

function App() {

  return (
    <div className={styles.app}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<Wellcome />} />
            <Route path="camera" element={<Camera />} />
            <Route path="map" element={<Map />} />
            <Route path="gallery" element={<Gallery />} />
            <Route path="achievements" element={<Achievements />} />
            <Route path="animal/:id" element={<AnimalDetail />} />
            <Route path="catch/:id" element={<Catch />} />
            <Route path="ar/:id" element={<AR />} />
            <Route path="quest" element={<ARquest />} />
            <Route path="shop" element={<Shop />} />
            <Route path="*" element={<Wellcome />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
