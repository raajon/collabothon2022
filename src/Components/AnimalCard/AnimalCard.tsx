import React from 'react';
import { useTranslation } from 'react-i18next';
import { Badge, Card } from 'react-bootstrap';
import { Link } from "react-router-dom";
import styles from './AnimalCard.module.css';

import { Animal } from '../../Utils/Types';

const AnimalCard = (props: Props) => {

  const { t } = useTranslation();

  let card;
  if(props.animal.level){
    card = (
      <Card className={styles.card} as={Link} to={"/animal/" + props.animal.id}>
        <Card.Img variant="top" src={require("../../Assets/img/animals/" + props.animal.key + ".jpg")} />
        <Badge className={styles.level} bg="info">{t('gallery.level')} {props.animal.level}</Badge>
        <Card.Body className={styles.cardBody}>
          <Card.Text className={styles.cardText}>
            {t('animal.'+props.animal.key+'.name')}
          </Card.Text>
        </Card.Body>
      </Card>
    )
  }else{
    card = (
      <Card className={styles.card}>
        <Card.Img variant="top" src={require("../../Assets/img/animals/undiscover.jpg")} />
        <Card.Body className={styles.cardBody}>
          <Card.Text className={styles.cardText}>
            {t('animal.undiscover.name')}
          </Card.Text>
        </Card.Body>
      </Card>
    )
  }

  return (
    <>
    {card}
    </>
  );
};

interface Props{
  animal:Animal;
}

export default AnimalCard;
