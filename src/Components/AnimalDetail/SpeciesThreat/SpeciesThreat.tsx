import React from 'react';
import { useTranslation } from 'react-i18next';
import { Col, Row, OverlayTrigger, Tooltip } from 'react-bootstrap';

import styles from './SpeciesThreat.module.css';
import { Animal } from '../../../Utils/Types';

const SpeciesThreat = (props:Props) => {

  const { t } = useTranslation();

  const categores = [
    "DD",
    "LC",
    "NT",
    "VU",
    "EN",
    "CR",
    "EW",
    "EX"
  ]

  return (
    <>
      <Row xs={8}>
        {categores.map( (c, i) => {
          if(c===props.animal.speciesThreat){
            return <Col key={i} className={styles.colSelected}>{c}</Col>
          }else{
            return <Col key={i} className={styles.col}>{c}</Col>
          }
        })}
      </Row>
      <Row xs={12}>
        <Col xs={12}>{t('ui.speciesThreat.'+props.animal.speciesThreat)}</Col>
      </Row>
    </>
  );
};

interface Props{
  animal:Animal;
}


  // return <Col key={i} className={styles.colSelected}>
  //   <OverlayTrigger placement="top" overlay={
  //               <Tooltip id={i}>
  //                 Tooltip on <strong>{placement}</strong>.
  //               </Tooltip>
  //             }>{c}
  //   </OverlayTrigger>
  // </Col>

export default SpeciesThreat;
