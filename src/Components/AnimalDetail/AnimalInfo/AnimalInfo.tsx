import React from 'react';
import { useTranslation } from 'react-i18next';
import { Col, Row } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faScaleBalanced, faRulerVertical, faRulerHorizontal } from '@fortawesome/free-solid-svg-icons'

import { Animal } from '../../../Utils/Types';
import styles from './AnimalInfo.module.css'

const AnimalInfo = (props:Props) => {

  const { t } = useTranslation();


  return (
    <>
      <Row className={styles.card}>
        <Col>
          <b>{t('ui.speciesThreat.howToHelp')}: </b>
          <ul>
            {props.animal.couseOfHazard.map((ch,i) => <li key={i}>{t('ui.speciesThreat.'+ch)}</li>)}
            <li>
              {t('ui.speciesThreat.supportingOrganizations')}
              {props.animal.supportingOrganizations.map((so,i)=><a key={i} href={so.url}>{so.name}</a>)}
            </li>
          </ul>
        </Col>
      </Row>
      <Row className={styles.card}>
        <Col xs={12}><b>{t('ui.animalInfo.cluster')}: </b>{t('animal.'+props.animal.key+'.cluster')}</Col>
        <Col xs="auto"><FontAwesomeIcon icon={faScaleBalanced} /> {t('animal.'+props.animal.key+'.weight')}</Col>
        <Col xs="auto"><FontAwesomeIcon icon={faRulerVertical} /> {t('animal.'+props.animal.key+'.length')}</Col>
        <Col xs="auto"><FontAwesomeIcon icon={faRulerHorizontal} /> {t('animal.'+props.animal.key+'.width')}</Col>
      </Row>
      <Row className={styles.card}>
        <Col>
          <b>{t('ui.animalInfo.desc')}: </b>
          {t('animal.'+props.animal.key+'.desc').split("<br/>").map((d,i) => <p key={i} className={styles.desc}>{d}</p>)}
        </Col>
      </Row>
    </>
  );
};

interface Props{
  animal:Animal;
}

export default AnimalInfo;
