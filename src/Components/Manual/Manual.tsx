import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Button, Carousel, Modal} from 'react-bootstrap';

const Manual = () => {

    const { t } = useTranslation();

    const [show, setShow] = useState(JSON.parse(localStorage.getItem("firstTime") || "true"));

    const carousel = [0,1,2];

    const startHandle = () =>{
      setShow(false);
      localStorage.setItem("firstTime", "false");
    }

  return (
    <Modal show={show} backdrop="static" keyboard={false}>
      <Modal.Body>
        <h1>{t('manual.wellcome')}</h1>
        <Carousel>
          {carousel.map(c=>(
            <Carousel.Item key={c}>
              <img
                className="d-block w-100"
                src={require("../../Assets/img/carousel/"+c+".jpg")}
                alt="First slide"
              />
              <Carousel.Caption>
                <h3>{t('manual.slideTitle.'+c)}</h3>
                <p>{t('manual.slideDesc.'+c)}</p>
              </Carousel.Caption>
            </Carousel.Item>
          ))}
        </Carousel>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" size="lg" onClick={()=> startHandle()}>{t('manual.startBtn')}</Button>
      </Modal.Footer>
    </Modal>
  )

}

export default Manual;
