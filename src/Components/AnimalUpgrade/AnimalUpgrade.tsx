import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from "react-router-dom";
import { Col, Image, Modal, Row } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowRightLong } from '@fortawesome/free-solid-svg-icons'

import { Animal } from '../../Utils/Types';

import styles from './AnimalUpgrade.module.css';

const AnimalUpgrade = (props:Props) => {

  const { t } = useTranslation();
  const navigate = useNavigate();

  const [show, setShow] = useState(false);
  const [animalsToShowIdx, setAnimalsToShowIdx] = useState(0);

  useEffect(()=>{
    if(props.animalsToShow.length>0){
      setShow(true);
    }
  },[
    props.animalsToShow
  ])

  const nextUpgrade = () => {
    if(animalsToShowIdx < (props.animalsToShow.length-1)){
      setAnimalsToShowIdx(animalsToShowIdx+1);
    }else{
      navigate("/gallery");
    }
  }
  const animal = props.animalsToShow[animalsToShowIdx];

  const modal = animal && (
    <Modal show={show} backdrop="static" keyboard={false}>
      <Modal.Body onClick={nextUpgrade}>
        <Row>
          <Col><h2 className={styles.title}>{t('animal.'+animal.key+'.name')}</h2></Col>
        </Row>
        <Row>
          <Col className={styles.imageHolder}>
            <Image fluid thumbnail src={require("../../Assets/img/animals/" + animal.key + ".jpg")}/>
          </Col>
        </Row>
      </Modal.Body>
      <Modal.Footer onClick={nextUpgrade}>
        <p className={styles.upgradeStat}>
          {animal.level} level
          <FontAwesomeIcon icon={faArrowRightLong} />
          {animal.level + 1} level
        </p>
      </Modal.Footer>
    </Modal>
  )

  return (
    <>
    {modal}
    </>
  );
};


interface Props{
  animalsToShow:Animal[];
}


export default AnimalUpgrade;
