import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from "react-router-dom";
import { useDispatch } from 'react-redux';
import { upgradeAnimal } from '../../Redux/animalSlice';
import { Button, Col, Row } from 'react-bootstrap';

import { Animal } from '../../Utils/Types';
import AnimalUpgrade from '../AnimalUpgrade/AnimalUpgrade';

import styles from './Quiz.module.css';

const Catch = (props:Props) => {

  const { t } = useTranslation();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [ question, setQuestion ] = useState(0);
  const [ points, setPoints ] = useState(0);
  const [ win, setWin ] = useState(false);
  const [animalsToShow, setAnimalsToShow] = useState([] as Animal[])

  useEffect(()=>{
    if(points>=3){
      setWin(true);
    }
  },[points]);

  const selectAnwser = (anwser:number) =>{
    if(props.animal.anwsers[question] === anwser){
      setPoints(points+1);
      if(points>=3){
        setWin(true);
      }
    }
    setQuestion(question+1);
  }

  const winHandle = () =>{
    dispatch(upgradeAnimal(props.animal.id));
    setAnimalsToShow([{...props.animal}]);
  }

  const looseHandle = () =>{
    navigate('/camera/');
  }

  let anwsers;
  let questions;
  if(question<3){
    anwsers = ["A", "B", "C"].map((a, i) => (
      <div key={i} className={styles.anwsers}>
        <Button variant="primary" className={styles.anwserBtn} size="sm" onClick={()=> selectAnwser(i)}>{a}</Button>
        {t('animal.'+props.animal.key+'.quiz'+ question + '.anwser' + i)}
      </div>
    ))

    questions = (
      <>
        <Row className={styles.card}>
          <Col>
            <h3>{t('animal.'+props.animal.key+'.quiz'+ question + '.question')}</h3>
          </Col>
        </Row>
        <Row className={styles.card}>
          <Col>
            {anwsers}
          </Col>
        </Row>
      </>
    );
  }

  const quizWin = (
    <>
      <Row className={styles.card}>
        <Col>
          <h3>{t('ui.quiz.win')}</h3>
        </Col>
        <Col xs={12} className="d-grid gap-2">
          <Button variant="primary" size="lg" onClick={()=> winHandle()}>{t('ui.quiz.btnWin')}</Button>
        </Col>
      </Row>
      <AnimalUpgrade animalsToShow={animalsToShow}/>
    </>
  )

  const quizLoose = (
    <>
      <Row className={styles.card}>
        <Col>
          <h3>{t('ui.quiz.loose')}</h3>
        </Col>
        <Col xs={12} className="d-grid gap-2">
          <Button variant="primary" size="lg" onClick={()=> looseHandle()}>{t('ui.quiz.btnLoose')}</Button>
        </Col>
      </Row>
    </>
  )

  return (
    <>
      {question<3 && questions}
      {question>=3 && win && quizWin}
      {question>=3 && !win && quizLoose}
    </>
  );
};

interface Props{
  animal: Animal;
}

export default Catch;
