// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCP8csd5o1eVA6lr1rpLmD9t4JaJIrWSI8",
  authDomain: "collabothon22.firebaseapp.com",
  projectId: "collabothon22",
  storageBucket: "collabothon22.appspot.com",
  messagingSenderId: "60821259078",
  appId: "1:60821259078:web:17c4ff3e56f5e260609b47",
  measurementId: "G-DG98B71VS4"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);