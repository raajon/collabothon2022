import React from "react";
import { AbstractMesh,
  Color3,
  Color4,
  // Engine,
  FreeCamera,
  HemisphericLight,
  Mesh,
  MeshBuilder,
  Scene,
  SceneLoader,
  StandardMaterial,
  Tools,
  WebXRBackgroundRemover,
  Vector3 } from "@babylonjs/core";
import { AdvancedDynamicTexture, Button, StackPanel } from "@babylonjs/gui";
import { useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { nextQuestPart } from '../Redux/questSlice';
import { useTranslation } from 'react-i18next';
import SceneComponent from 'babylonjs-hook'; // if you install 'babylonjs-hook' NPM.
import 'babylonjs-loaders';

import "./AR.css";
import { useWindowDimensions } from '../Utils/Hooks';
import { Quest } from '../Utils/Types';

const ARquest = () => {

  const questData:Quest = useSelector((state:any) => state.questData.questData);
  const questScene = questData.scene[questData.activeScene];
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const {width, height} = useWindowDimensions();
  const treesModel:AbstractMesh[] = [];

  const loadTrees = async(scene: Scene) =>{
      const rawTrees = [];
      rawTrees.push(await SceneLoader.ImportMeshAsync("", "/env/", "tree1.glb", scene));
      rawTrees.push(await SceneLoader.ImportMeshAsync("", "/env/", "tree2.glb", scene));
      rawTrees.push(await SceneLoader.ImportMeshAsync("", "/env/", "tree3.glb", scene));
      rawTrees.push(await SceneLoader.ImportMeshAsync("", "/env/", "tree4.glb", scene));
      rawTrees.push(await SceneLoader.ImportMeshAsync("", "/env/", "tree5.glb", scene));
      rawTrees.push(await SceneLoader.ImportMeshAsync("", "/env/", "tree6.glb", scene));
      rawTrees.push(await SceneLoader.ImportMeshAsync("", "/env/", "bush.glb", scene));
      rawTrees.forEach(tree => {
        const meshes = tree.meshes[0];
        treesModel.push(meshes)
        meshes.position = new Vector3(0,-100, 0);
        meshes.scaling = new Vector3(.2, .2, .2);
        meshes.isPickable = false;
      });
  }

  const loadAnimal = async(scene: Scene) =>{
      for(let i=0; i<questScene.ar.models.length; i++){
        const ar = questScene.ar.models[i];
        const model:AbstractMesh = (await SceneLoader.ImportMeshAsync("", "/models/", ar.model+".glb", scene)).meshes[0];
        model.position = new Vector3(ar.position.x, ar.position.y, ar.position.z);
        model.scaling = new Vector3(ar.scale, ar.scale, ar.scale)
      }
  }

  const loadDialog = (scene: Scene) =>{
    const plane = Mesh.CreatePlane("plane", 2, scene);
    plane.position = new Vector3(0,1.2,2.5);

    const advancedTexture2 = AdvancedDynamicTexture.CreateForMesh(plane);

    const dialog = Button.CreateSimpleButton("but1", t(questScene.ar.dialog));
    dialog.width = .4;
    dialog.height = 0.2;
    dialog.color = "black";
    dialog.fontSize = 30;
    dialog.background = "white";
    dialog.onPointerUpObservable.add(function() {
        dispatch(nextQuestPart());
        navigate(-1);
    });
    advancedTexture2.addControl(dialog);
  }

  const onSceneReady = (scene:Scene) => {
    const e = scene.getEngine()
    // setEngine(e)
    const canvas = e.getRenderingCanvas();
    scene.clearColor = new Color4(.3, .4, 1, 1)

    const c = new FreeCamera("camera1", new Vector3(0, 0.5, 0), scene);
    c.setTarget(new Vector3(0,.5,3));
    c.attachControl(canvas, true);
    c.minZ = 0.1;
    c.maxZ = 250;
    // setCamera(c)
    var light = new HemisphericLight("light1", new Vector3(.9, 1, 0), scene);
    light.intensity = 0.7;

    const backgroundMeshes:AbstractMesh[] = [];

    loadAnimal(scene);
    loadDialog(scene);
    loadTrees(scene).then(()=>{
      for(let i=0; i<100; i++){
          const tree = treesModel[Math.floor(Math.random()*7)].clone('tree-'+i, null);
          const rad = Math.random() * 2 * Math.PI;
          const x = (2 + Math.random()*3) * Math.cos(rad);
          const z = 1.5 + (2 + Math.random()*3) * Math.sin(rad);
          tree!.position = new Vector3(x, 0, z)
          const s = .15 + (Math.random()*.1)
          tree!.scaling = new Vector3(.2, s, .2);
          backgroundMeshes.push(tree!);
      }
    });

    const ground = MeshBuilder.CreateGround("ground", {width: 500, height: 500}, scene);
    var groundMaterial = new StandardMaterial("groundMaterial", scene);
    groundMaterial.diffuseColor = new Color3(.3, .5, .2);
    ground.material = groundMaterial;
    backgroundMeshes.push(ground);

    var advancedTexture = AdvancedDynamicTexture.CreateFullscreenUI("UI");

    // const exitButton = Button.CreateImageOnlyButton("exitButton", "./sprites/exitButton.png");
    const exitButton = Button.CreateSimpleButton("but", "X");
    exitButton.width = width*.2+"px";
    exitButton.height = width*.2+"px";
    exitButton.color = "white";
    exitButton.cornerRadius = width*.4;
    exitButton.paddingRight = '20px';
    exitButton.paddingTop = '20px';
    exitButton.verticalAlignment = StackPanel.VERTICAL_ALIGNMENT_TOP;
    exitButton.horizontalAlignment = StackPanel.HORIZONTAL_ALIGNMENT_RIGHT;
    exitButton.onPointerUpObservable.add(() => {
      navigate(-1);
    });
    advancedTexture.addControl(exitButton);

    var photoButton = Button.CreateSimpleButton("but", "photo");
    photoButton.width = width*.2+"px";
    photoButton.height = width*.2+"px";
    photoButton.color = "white";
    exitButton.cornerRadius = width*.4;
    photoButton.verticalAlignment = StackPanel.VERTICAL_ALIGNMENT_BOTTOM;
    photoButton.horizontalAlignment = StackPanel.HORIZONTAL_ALIGNMENT_CENTER;
    photoButton.onPointerUpObservable.add(() => {
      Tools.CreateScreenshot(e!, c!, {width:width, height:height});
    });
    advancedTexture.addControl(photoButton);

    scene.createDefaultXRExperienceAsync({
      // ask for an ar-session
      uiOptions: {
        sessionMode: "immersive-ar",
      },
    }).then((xr)=>{
      const fm = xr.baseExperience.featuresManager;

      const xrBackgroundRemover = fm.enableFeature(WebXRBackgroundRemover, "latest", {
        backgroundMeshes: backgroundMeshes,
        environmentHelperRemovalFlags: {
          skyBox: true,
          ground: true,
        },
      });
    });
  };

  const onRender = (scene:Scene) => {
  };

  // const createScreenshot = () =>{
  //   Tools.CreateScreenshot(engine!, camera!, {width:width, height:height});
  // }

  return(
    <div>
      <SceneComponent antialias onSceneReady={onSceneReady} onRender={onRender} id="my-canvas" />
    </div>
  )
};

export default ARquest;
