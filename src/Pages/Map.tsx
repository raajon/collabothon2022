import React, { useEffect, useState } from "react";
import { AbstractMesh,
  ArcRotateCamera,
  Color3,
  Color4,
  HemisphericLight,
  Mesh,
  Scene,
  SceneLoader,
  StandardMaterial,
  Vector3 } from "@babylonjs/core";
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import SceneComponent from 'babylonjs-hook'; // if you install 'babylonjs-hook' NPM.
import 'babylonjs-loaders';
import { Button, Col, Row } from 'react-bootstrap';

import "./Map.css";
import { Animal, Quest } from '../Utils/Types';
import useGeolocation from 'react-hook-geolocation';
import { global2local, getDistance } from '../Utils/Calc';

const Map = () => {

  const [animalToCatch, setAnimalToCatch] = useState<Animal>()
  const [questToCatch, setQuestToCatch] = useState(0)
  const animalData:Animal[] = useSelector((state:any) => state.animalData.animalData).filter((a:Animal)=>a.location === 'lodz');
  const questData:Quest = useSelector((state:any) => state.questData.questData);
  const questScene = questData.scene[questData.activeScene];
  const navigate = useNavigate();
  const { t } = useTranslation();
  const geolocation = useGeolocation();

  // console.log(questData)

  useEffect(()=>{
    localStorage.setItem("geolocation", JSON.stringify(geolocation));
    let nearestAnimal = {animal:{}as Animal, distance:1000000}
    animalData.forEach(animal => {
      const distance = getDistance(geolocation.longitude, animal.longitude, geolocation.latitude, animal.latitude);
      if(distance<nearestAnimal.distance){
        nearestAnimal.animal = animal;
        nearestAnimal.distance = distance;
      }
      if(nearestAnimal.distance<50){
        setAnimalToCatch(nearestAnimal.animal);
      }
    });

    if(questData?.active){
      const questDistance = getDistance(geolocation.longitude, questScene.longitude, geolocation.latitude, questScene.latitude);
      if(questDistance<10){
        setQuestToCatch(questData.activeScene);
      }
    }

  },[geolocation, animalData, questData, questScene])

  const loadMap = async(scene: Scene) =>{
      const model:AbstractMesh = (await SceneLoader.ImportMeshAsync("", "/map/", "lodz.babylon", scene)).meshes[0];
      model.position = new Vector3(0,0,0);
  }

  const loadAnimals = async(scene: Scene) =>{
    for(let i=0; i<animalData.length; i++){
      const animal = animalData[i];
      const model:AbstractMesh = (await SceneLoader.ImportMeshAsync("", "/models/", animal.model+".glb", scene)).meshes[0];
      model.position = global2local(animal.latitude, animal.longitude);
    }
  }

  const loadQuest = async(scene: Scene) =>{
    console.log(questData)
    if(questData && questData.active){
      const questScene = questData.scene[questData.activeScene];
      const model:AbstractMesh = (await SceneLoader.ImportMeshAsync("", "/models/", questScene.model+".glb", scene)).meshes[0];
      model.position = global2local(questScene.latitude, questScene.longitude);

      const questMaterial = new StandardMaterial("groundMaterial", scene);
      questMaterial.diffuseColor = new Color3(1, 0, 0);

      const dot = Mesh.CreateSphere("player", 16, .2, scene);
      dot.material = questMaterial;
      dot.position = global2local(questScene.latitude, questScene.longitude);
      dot.position.y = 2;

      const dash = Mesh.CreateSphere("player", 16, .2, scene);
      dash.material = questMaterial;
      dash.position = global2local(questScene.latitude, questScene.longitude);
      dash.position.y = 2.5;
      dash.scaling.y = 3;
    }
  }

  const onSceneReady = (scene:Scene) => {
    const engine = scene.getEngine();
    const canvas = engine.getRenderingCanvas();
    scene.clearColor = new Color4(.3, .4, 1, 1)

    const camera = new ArcRotateCamera("Camera", 0, 0, 5, new Vector3(0, 3, 0), scene);
    camera.attachControl(canvas, true);
    camera.minZ = 0.1;
    camera.maxZ = 250;
    camera.inputs.removeByType("FreeCameraKeyboardMoveInput");

    var light = new HemisphericLight("light1", new Vector3(.9, 1, 0), scene);
    light.intensity = 0.7;

    const player = Mesh.CreateSphere("player", 16, 1, scene);
    var playerMaterial = new StandardMaterial("groundMaterial", scene);
    playerMaterial.diffuseColor = new Color3(1, 0, 0);
    player.material = playerMaterial;
    player.position = new Vector3(0,1,0);

    loadMap(scene);
    loadAnimals(scene);
    loadQuest(scene);
  };

  const onRender = (scene:Scene) => {
    const geolocation = JSON.parse(localStorage.getItem("geolocation") || "{}");

    const camera:ArcRotateCamera = scene.cameras[0] as ArcRotateCamera;
    const position = global2local(geolocation.latitude, geolocation.longitude);
    position.y = 1;
    camera.setTarget(position.clone());
    scene.meshes[0].position = position.clone();
    // scene.meshes[0].position.y = 1;
  };

  const unclock = () =>{
    if(animalToCatch){
      navigate("/catch/"+animalToCatch.id);
    }
  }

  const goToQuestAR = () =>{
    if(questToCatch){
      navigate("/quest");
    }
  }

  return(
    <div>
      {animalToCatch &&
        <div className="catch">
          <Row>
            <Col xs={9}>
              {t('animal.'+animalToCatch.key+'.name')}
            </Col>
            <Col xs={3}>
              <Button variant="success" size="sm" onClick={unclock}>{t('camera.unlockBtn')}</Button>
            </Col>
          </Row>
        </div>}
        {questToCatch &&
          <div className="catchQuest">
            <Row>
              <Col xs={9}>
                {t('quest.start')}
              </Col>
              <Col xs={3}>
                <Button variant="success" size="sm" onClick={goToQuestAR}>{t('quest.go')}</Button>
              </Col>
            </Row>
          </div>}
      <SceneComponent antialias onSceneReady={onSceneReady} onRender={onRender} id="my-canvas" />
    </div>
  )
};

export default Map
