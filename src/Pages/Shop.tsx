import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import { Badge, ListGroup } from 'react-bootstrap';
import { upgradeAnimal } from '../Redux/animalSlice';

import { deepClone } from '../Utils/Helpers';
import AnimalUpgrade from '../Components/AnimalUpgrade/AnimalUpgrade';
import { Animal, ShopItem } from '../Utils/Types';

import styles from './Pages.module.css';

const Achievements = () => {
  const [animalsToShow, setAnimalsToShow] = useState([] as Animal[])
  const { t } = useTranslation();
  const animalData = useSelector((state:any) => state.animalData.animalData);
  const dispatch = useDispatch();

  const shopItems:ShopItem[] = [
    {name:"lodzZoo", price:5, count:2, criteria:{name:"location", value:"lodz"}},
    {name:"wroclawZoo", price:5, count:2, criteria:{name:"location", value:"wroclaw"}},
    {name:"warsawZoo", price:5, count:2, criteria:{name:"location", value:"warsaw"}}
  ]

  const getAnimals = (sI:ShopItem) =>{
    const animalsToUpgrade = deepClone(animalData.filter((a:Animal)=>a[sI.criteria.name] === sI.criteria.value));
    const animalsToShowState:Animal[] = [];

    for( let i=0; i<sI.count; i++){
      var animalToUpgrade = animalsToUpgrade[Math.floor(Math.random()*animalsToUpgrade.length)];
      animalsToShowState.push({...animalToUpgrade});
      animalToUpgrade.level++;
      dispatch(upgradeAnimal(animalToUpgrade.id));
    }
    setAnimalsToShow(animalsToShowState);
  }

  return (
    <>
      <h2 className={styles.title}>{t('ui.shop')}</h2>
      <ListGroup>
        {shopItems.map((sI,i)=>(
          <ListGroup.Item key={i} className="d-flex justify-content-between align-items-start" onClick={()=>getAnimals(sI)}>
            <div className="ms-2 me-auto">
              <div className="fw-bold">{t('shopItems.' + sI.name + '.name')}</div>
              {t('shopItems.' + sI.name + '.desc')}
            </div>
            <Badge bg="primary" pill> {sI.price} € </Badge>
          </ListGroup.Item>
        ))}
      </ListGroup>
      <AnimalUpgrade animalsToShow={animalsToShow}/>
    </>
  );
};

export default Achievements;
