import React from 'react';
// import { useTranslation } from 'react-i18next';
import { Col, Row } from 'react-bootstrap';


// import { useWindowDimensions } from '../Utils/Hooks';
import logo from '../Assets/img/logo.svg';

import styles from './Pages.module.css';

const Wellcome = () => {

  // const { t } = useTranslation();
  // const { height, width } = useWindowDimensions();

  return (
    <>
      <Row>
        <Col>
          <p className={styles.cityDruids}>City Druids</p>
          <p className={styles.presents}>presents</p>
        </Col>
      </Row>
      <img src={logo} className={styles.logo} alt="ZooGo Logo" />
    </>
  );
};

export default Wellcome;
