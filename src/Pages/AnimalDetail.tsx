import React from 'react';
import { useTranslation } from 'react-i18next';
import { Button, Col, Image, Row } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { Animal } from '../Utils/Types';
import SpeciesThreat from '../Components/AnimalDetail/SpeciesThreat/SpeciesThreat';
import AnimalInfo from '../Components/AnimalDetail/AnimalInfo/AnimalInfo';

import styles from './Pages.module.css';

const AnimalDetail = () => {

  type Params = {
    id: string;
  };

  const animalData:Animal[] = useSelector((state:any) => state.animalData.animalData);
  const { id } = useParams<Params>();
  const { t } = useTranslation();

  const animal = animalData[Number(id)];

  return (
    <>
      <Row>
        <Col><h2 className={styles.title}>{t('animal.'+animal.key+'.name')}</h2></Col>
      </Row>
      <Row>
        <Col className={styles.imageHolder}>
          <Image fluid thumbnail src={require("../Assets/img/animals/" + animal.key + ".jpg")}/>
          <Button as="a" href={"/ar/"+id} className={styles.btnOverImage}>AR</Button>
        </Col>
      </Row>
      <SpeciesThreat animal={animal}/>
      <AnimalInfo animal={animal}/>
    </>
  );
};

    // position: relative;
    // bottom: 50px;
    // left: 20px;
export default AnimalDetail;
