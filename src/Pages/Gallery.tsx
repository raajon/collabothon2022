import React from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { Button, Col, Row } from 'react-bootstrap';

import AnimalCard from '../Components/AnimalCard/AnimalCard';
import { Animal } from '../Utils/Types';

import styles from './Pages.module.css';

const Gallery = () => {

  const { t } = useTranslation();

  const animalData:Animal[] = useSelector((state:any) => state.animalData.animalData);

  const resetAnimalData = () =>{
    localStorage.removeItem("animalData");
    localStorage.removeItem("firstTime");
  }

  return (
    <>
      <h2 className={styles.title}>{t('ui.gallery')}</h2>
      <Row xs={2} md={4} className="g-4">
        {animalData.filter((a:Animal)=>a.level).map((a:Animal) => <Col key={a.id}><AnimalCard animal ={a}/></Col> )}
        {animalData.filter((a:Animal)=>!a.level).map((a:Animal) => <Col key={a.id}><AnimalCard animal ={a}/></Col> )}
      </Row>
      <Row>
        <Button variant="success" size="lg" onClick={resetAnimalData}>{t('camera.unlockBtn')}</Button>
      </Row>
    </>
  );
};

export default Gallery;
