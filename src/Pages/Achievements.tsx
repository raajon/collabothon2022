import React from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { Badge, ListGroup } from 'react-bootstrap';

import { Animal } from '../Utils/Types';

import styles from './Pages.module.css';

const Achievements = () => {

  const { t } = useTranslation();
  const animalData = useSelector((state:any) => state.animalData.animalData);

  const achievements = [
    {name:"collectFirst", getValueFun:()=>{ return animalData.find((ad:Animal)=>ad.level>0)?1:0 }, condition:1},
    {name:"10Animals", getValueFun:()=>{ return animalData.filter((ad:Animal)=>ad.level>0).length }, condition:10},
    {name:"polishHedgehog", getValueFun:()=>{ return animalData.filter((ad:Animal)=>ad.level>0 && ad.key.indexOf("Hedgehog")>=0).length }, condition:animalData.filter((ad:Animal)=> ad.key.indexOf("Hedgehog")>=0).length }
  ]

  console.log(achievements)

  return (
    <>
      <h2 className={styles.title}>{t('ui.achievements')}</h2>
      <ListGroup>
        {achievements.map((a,i)=>(
          <ListGroup.Item key={i} className="d-flex justify-content-between align-items-start">
            <div className="ms-2 me-auto">
              <div className="fw-bold">{t('achievements.' + a.name + '.name')}</div>
              {t('achievements.' + a.name + '.desc')}
            </div>
            {a.getValueFun()<a.condition && <Badge bg="primary" pill> {a.getValueFun()?1:0}/{a.condition} </Badge>}
          </ListGroup.Item>
        ))}
      </ListGroup>
    </>
  );
};

export default Achievements;
