import React from "react";
import { AbstractMesh,
  Color3,
  Color4,
  // Engine,
  FreeCamera,
  HemisphericLight,
  MeshBuilder,
  Scene,
  SceneLoader,
  StandardMaterial,
  Tools,
  WebXRBackgroundRemover,
  Vector3 } from "@babylonjs/core";
import { AdvancedDynamicTexture, Button, StackPanel } from "@babylonjs/gui";
import { useParams, useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import SceneComponent from 'babylonjs-hook'; // if you install 'babylonjs-hook' NPM.
import 'babylonjs-loaders';

import "./AR.css";
import { useWindowDimensions } from '../Utils/Hooks';
import { Animal } from '../Utils/Types';

const AR = () => {

  type Params = {
    id: string;
  };

  // const [engine, setEngine] = useState<Engine>();
  // const [camera, setCamera] = useState<FreeCamera>();
  const animalData:Animal[] = useSelector((state:any) => state.animalData.animalData);
  const navigate = useNavigate();
  const { id } = useParams<Params>();
  const {width, height} = useWindowDimensions();
  console.log(width, height)
  const animal = animalData[Number(id)];
  const treesModel:AbstractMesh[] = [];

  const loadTrees = async(scene: Scene) =>{
      const rawTrees = [];
      rawTrees.push(await SceneLoader.ImportMeshAsync("", "/env/", "tree1.glb", scene));
      rawTrees.push(await SceneLoader.ImportMeshAsync("", "/env/", "tree2.glb", scene));
      rawTrees.push(await SceneLoader.ImportMeshAsync("", "/env/", "tree3.glb", scene));
      rawTrees.push(await SceneLoader.ImportMeshAsync("", "/env/", "tree4.glb", scene));
      rawTrees.push(await SceneLoader.ImportMeshAsync("", "/env/", "tree5.glb", scene));
      rawTrees.push(await SceneLoader.ImportMeshAsync("", "/env/", "tree6.glb", scene));
      rawTrees.push(await SceneLoader.ImportMeshAsync("", "/env/", "bush.glb", scene));
      rawTrees.forEach(tree => {
        const meshes = tree.meshes[0];
        treesModel.push(meshes)
        meshes.position = new Vector3(0,-100, 0);
        meshes.scaling = new Vector3(.2, .2, .2);
        meshes.isPickable = false;
      });
  }

  const loadAnimal = async(scene: Scene) =>{
      const model:AbstractMesh = (await SceneLoader.ImportMeshAsync("", "/models/", animal.model+".glb", scene)).meshes[0];
      model.position = new Vector3(0,0,3);
      model.scaling = new Vector3(.5, .5, .5)
  }

  const onSceneReady = (scene:Scene) => {
    const e = scene.getEngine()
    // setEngine(e)
    const canvas = e.getRenderingCanvas();
    scene.clearColor = new Color4(.3, .4, 1, 1)

    const c = new FreeCamera("camera1", new Vector3(0, 0.5, 0), scene);
    c.setTarget(new Vector3(0,.5,3));
    c.attachControl(canvas, true);
    c.minZ = 0.1;
    c.maxZ = 250;
    // setCamera(c)
    var light = new HemisphericLight("light1", new Vector3(.9, 1, 0), scene);
    light.intensity = 0.7;

    const backgroundMeshes:AbstractMesh[] = [];

    loadAnimal(scene);
    loadTrees(scene).then(()=>{
      for(let i=0; i<100; i++){
          const tree = treesModel[Math.floor(Math.random()*7)].clone('tree-'+i, null);
          const rad = Math.random() * 2 * Math.PI;
          const x = (2 + Math.random()*3) * Math.cos(rad);
          const z = 1 + (2 + Math.random()*3) * Math.sin(rad);
          tree!.position = new Vector3(x, 0, z)
          const s = .15 + (Math.random()*.1)
          tree!.scaling = new Vector3(.2, s, .2);
          backgroundMeshes.push(tree!);
      }
    });

    const ground = MeshBuilder.CreateGround("ground", {width: 500, height: 500}, scene);
    var groundMaterial = new StandardMaterial("groundMaterial", scene);
    groundMaterial.diffuseColor = new Color3(.3, .5, .2);
    ground.material = groundMaterial;
    backgroundMeshes.push(ground);

    var advancedTexture = AdvancedDynamicTexture.CreateFullscreenUI("UI");

    // const exitButton = Button.CreateImageOnlyButton("exitButton", "./sprites/exitButton.png");
    const exitButton = Button.CreateSimpleButton("but", "X");
    exitButton.width = width*.2+"px";
    exitButton.height = width*.2+"px";
    exitButton.color = "white";
    exitButton.cornerRadius = width*.4;
    exitButton.paddingRight = '20px';
    exitButton.paddingTop = '20px';
    exitButton.verticalAlignment = StackPanel.VERTICAL_ALIGNMENT_TOP;
    exitButton.horizontalAlignment = StackPanel.HORIZONTAL_ALIGNMENT_RIGHT;
    exitButton.onPointerUpObservable.add(() => {
      navigate(-1);
    });
    advancedTexture.addControl(exitButton);

    var photoButton = Button.CreateSimpleButton("but", "photo");
    photoButton.width = width*.2+"px";
    photoButton.height = width*.2+"px";
    photoButton.color = "white";
    exitButton.cornerRadius = width*.4;
    photoButton.verticalAlignment = StackPanel.VERTICAL_ALIGNMENT_BOTTOM;
    photoButton.horizontalAlignment = StackPanel.HORIZONTAL_ALIGNMENT_CENTER;
    photoButton.onPointerUpObservable.add(() => {
      Tools.CreateScreenshot(e!, c!, {width:width, height:height});
    });
    advancedTexture.addControl(photoButton);

    scene.createDefaultXRExperienceAsync({
      // ask for an ar-session
      uiOptions: {
        sessionMode: "immersive-ar",
      },
    }).then((xr)=>{
      const fm = xr.baseExperience.featuresManager;

      const xrBackgroundRemover = fm.enableFeature(WebXRBackgroundRemover, "latest", {
        backgroundMeshes: backgroundMeshes,
        environmentHelperRemovalFlags: {
          skyBox: true,
          ground: true,
        },
      });
    });
  };

  const onRender = (scene:Scene) => {
  };

  // const createScreenshot = () =>{
  //   Tools.CreateScreenshot(engine!, camera!, {width:width, height:height});
  // }

  return(
    <div>
      <SceneComponent antialias onSceneReady={onSceneReady} onRender={onRender} id="my-canvas" />
    </div>
  )
};

export default AR
