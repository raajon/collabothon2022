import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from "react-router-dom";
import { useSelector } from 'react-redux';
import { Button, Col, Row } from 'react-bootstrap';
import { QrReader } from 'react-qr-reader';
import useGeolocation from 'react-hook-geolocation';


import { Animal } from '../Utils/Types';
import SpeciesThreat from '../Components/AnimalDetail/SpeciesThreat/SpeciesThreat';
import styles from './Pages.module.css';

const Camera = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const geolocation = useGeolocation();
  const [data, setData] = useState('No result');
  const [id, setId] = useState(0);
  const [animal, setAnimal] = useState<Animal | undefined>(undefined);

  const animalData:Animal[] = useSelector((state:any) => state.animalData.animalData);

  useEffect(()=>{
    if( data.includes("https://collabothon22.web.app/catch/") ||
        data.includes("https://192.168.24.10:3000/catch/") ||
        data.includes("https://zoogo.polarny.it/catch/") ){
        setId(parseInt(data.split("/")[data.split("/").length-1]));
        setAnimal(animalData[Number(id)]);
    }
  },[
    data, animalData, id
  ])

  const unclock = () =>{
    if(animal){
      navigate("/catch/"+animal.id);
    }
  }

  const resetFirtTime = () =>{
    localStorage.setItem("firstTime", "true");
  }

  const manual = !animal && (
    <Row className={styles.card}>
      <Col>
        <p>{t('camera.manual')}</p>
      </Col>
    </Row>
  )

  const animalToUnclock = !!animal &&(
    <>
      <Row>
        <Col>
          <h2 className={styles.title}>{t('animal.'+animal.key+'.name')}</h2>
        </Col>
      </Row>
      <SpeciesThreat animal={animal}/>
      <Row>
        <Col className="d-grid gap-2">
          <Button variant="success" size="lg" onClick={unclock}>{t('camera.unlockBtn')}</Button>
        </Col>
      </Row>
    </>
  )


  return (
    <>
      <h2 className={styles.title}>{t('ui.camera')}</h2>
      <Row>
        <Col>
          <QrReader
            constraints={ {facingMode: 'environment'} }
            onResult={(result, error) => {
              if (!!result) {
                setData(result['text']);
              }

              if (!!error) {
                // console.info(error);
              }
            }}
          />
        </Col>
      </Row>
      {manual}
      {animalToUnclock}
      <Row>
        <Col>
          {!animal && <Button variant="primary" onClick={()=>setData("https://localhost:3000/catch/0")}>simulate</Button>}
          <Button variant="success" size="lg" onClick={resetFirtTime}>Reset firstTime</Button>
        </Col>
      </Row>
      <Row>
        <Col>
          {!geolocation.error
              ? (
                <ul>
                  <li>Latitude:          {geolocation.latitude}</li>
                  <li>Longitude:         {geolocation.longitude}</li>
                  <li>Location accuracy: {geolocation.accuracy}</li>
                  <li>Altitude:          {geolocation.altitude}</li>
                  <li>Altitude accuracy: {geolocation.altitudeAccuracy}</li>
                  <li>Heading:           {geolocation.heading}</li>
                  <li>Speed:             {geolocation.speed}</li>
                  <li>Timestamp:         {geolocation.timestamp}</li>
                </ul>
              )
              : (
                <p>No geolocation, sorry.</p>
              )}
        </Col>
      </Row>
    </>
  );
};

export default Camera;
