import React from 'react';
import { useTranslation } from 'react-i18next';
import { Col, Image, Row } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { Animal } from '../Utils/Types';
import SpeciesThreat from '../Components/AnimalDetail/SpeciesThreat/SpeciesThreat';
import Quiz from '../Components/Quiz/Quiz';

import styles from './Pages.module.css';

const Catch = () => {

  type Params = {
    id: string;
  };

  const animalData:Animal[] = useSelector((state:any) => state.animalData.animalData);
  const { id } = useParams<Params>();
  const { t } = useTranslation();


  const animal = animalData[Number(id)];

  return (
    <>
      <Row>
        <Col><h2 className={styles.title}>{t('animal.'+animal.key+'.name')}</h2></Col>
      </Row>
      <Row>
        <Col><Image fluid thumbnail src={require("../Assets/img/animals/" + animal.key + ".jpg")}/></Col>
      </Row>
      <SpeciesThreat animal={animal}/>
      <Quiz animal={animal}/>
    </>
  );
};

export default Catch;
